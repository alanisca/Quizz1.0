async function createQuiz(){
    const nivel = $('#nivel').val();
    const grado = $('#grado').val();
    const materia = $('#materia').val();
    const bloque = $('#bloque').val();
    const secuencia = $('#secuencia').val();
    const nombre = $('#nombre').val();
    const intentos = $('#intentos').val();

    /* Eliminar imágenes al editar quiz */
    //Al cargar el quiz a editar se llena el arreglo "id_img" con el id de las imagenes que existen
    //se agrega ese id al input de las imágenes con el img-id
    for (let index = 0; index < id_img.length; index++) {
        $('.loading .msg-cargando').html(`Verificando ${id_img.length} imágenes ${index+1}`);
        //Si se elimina la pregunta el id ya no se encontrará por lo cual se elimina esa imagen
        if(id_img[index] != ''){
            if($(`input[img-id="${id_img[index]}"]`).length < 1) {//Se eliminó el quiz que contenía esta imagen
                $('.loading .msg-cargando').html(`Eliminando imagen de pregunta borrada ${index+1}`);
                await deleteImage(id_img[index]);//Se elimina la imagen
                console.log("Se eliminó la pregunta");
                //Se limpia el input de la imagen anterior para poder cargar la imagen nueva si es necesario
                $(`input[img-id="${id_img[index]}"]`).removeAttr('img-id');//Se quita el id de la imagen previa
                $(`input[img-id="${id_img[index]}"]`).removeAttr('data-default-file');//Quita la imagen actual
            }
            else{
                //Si el id existe pero tiene la clase img_dele quiere decir que se eliminó la imagen
                if($(`input[img-id="${id_img[index]}"]`).hasClass('img_deleted')){
                    $('.loading .msg-cargando').html(`Eliminando imagen que se eliminó ${index+1}`);
                    await deleteImage(id_img[index]);//Se elimina la imagen
                    console.log("Se eliminó la imagen");
                    //Se limpia el input de la imagen anterior para poder cargar la imagen nueva si es necesario
                    $(`input[img-id="${id_img[index]}"]`).removeAttr('img-id');//Se quita el id de la imagen previa
                    $(`input[img-id="${id_img[index]}"]`).removeAttr('data-default-file');//Quita la imagen actual
                }
                else{
                    if($(`input[img-id="${id_img[index]}"]`).val() != ''){//Existe pero se cambió de imagen
                        $('.loading .msg-cargando').html(`Eliminando imagen que se cambió ${index+1}`);
                        await deleteImage(id_img[index]);//Se elimina la imagen
                        console.log("Se cambió la imagen");
                        //Se limpia el input de la imagen anterior para poder cargar la imagen nueva
                        $(`input[img-id="${id_img[index]}"]`).removeAttr('img-id');//Se quita el id de la imagen previa
                        $(`input[img-id="${id_img[index]}"]`).removeAttr('data-default-file');//Quita la imagen actual
                    }
                }
            }
        }
    }
    var arrPreguntas = await loadQuestions();
    var newQuizArr = {
        cuestionario: arrPreguntas,
        bloque: bloque,
        contestado: false,
        creador: 'Pendiente',
        estado: 'por revisar',
        grado: grado,
        grupo: 'Creado por Especialista',
        intentos: intentos,
        materia: materia,
        nivel: nivel,
        nombreQuizz: nombre,
        secuencia: secuencia,
    }
    console.log('FINAL',newQuizArr,arrPreguntas);
    var n_url = '';
    if(quizType == 'new') n_url = '/quiz/saveNewQuizz';
    if(quizType == 'edit') n_url = '/quiz/saveEditQuizz/'+quizID;
    $.ajax({
        type: 'POST',
        url: window.location.origin+n_url,
        data: {'quiz': newQuizArr},
        success: function(response) {
            console.log('Respuesta success',response);
            if(response.status == "ok"){
                window.location.replace(response.redirect);
            }
        },
        error: function(jqXHR, textStatus, errorMessage) {
            console.log('Respuesta error',errorMessage);
        }
    });
}
async function loadQuestions(){
    var arrPreguntas = [];
    for (i = 0; i < $(".card.quizz").length; i++) {
        var dataId = $(".card.quizz").eq( i ).attr("data-id");
        var typeQuiz = $(".card.quizz").eq( i ).attr('data-quiz');
        switch (typeQuiz) {
            case 'pregunta_abierta':
                $('.loading .msg-cargando').html(`Cargando pregunta abierta ${i+1} de ${$(".card.quizz").length}`);
                arrPreguntas.push(await newQuizPreguntaAbierta(dataId));
                break;
            case 'pregunta_relacional':
                $('.loading .msg-cargando').html(`Cargando pregunta relacional ${i+1} de ${$(".card.quizz").length}`);
                arrPreguntas.push(await newQuizPreguntaRelacional(dataId));
                break;
            case 'pregunta_truefalse':
                $('.loading .msg-cargando').html(`Cargando pregunta truefalse ${i+1} de ${$(".card.quizz").length}`);
                arrPreguntas.push(await newQuizPreguntaTF(dataId));
                break;
            case 'pregunta_arrastrar_palabras':
                $('.loading .msg-cargando').html(`Cargando pregunta arrastrar palabras ${i+1} de ${$(".card.quizz").length}`);
                arrPreguntas.push(await newQuizArrastrarP(dataId));
                break;
            case 'pregunta_preguntaSeleccionar':
                $('.loading .msg-cargando').html(`Cargando pregunta arrastrar palabras ${i+1} de ${$(".card.quizz").length}`);
                arrPreguntas.push(await newQuizseleccionarP(dataId));
                break;
            default:
                break;
        }
        console.log("quiz2",i);
    }
    return new Promise((resolve, reject) => { 
        resolve(arrPreguntas);
    });
}