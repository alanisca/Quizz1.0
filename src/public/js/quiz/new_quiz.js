$(document).ready(function () {
    $('.dropify').dropify({
        messages: {
            'default': 'Agregar archivo',
            'replace': 'Reemplazar archivo',
            'remove':  'Quitar archivo',
            'error':   'Ooops, algo salió mal.'
        }
    });
    getNiveles();
});
var cont_quiz = 0;
/* Datos del quiz */
function getNiveles(){
    $.getJSON("/pruebaAJAXniveles", function (niveles) {
        for (var i = 0; i < niveles.length; i++) {
            var nombres = (niveles[i].nombre);
            var id = (niveles[i].id);
            var select = document.getElementById("nivel").innerHTML += `<option value='${id}'>${nombres}</option>`;
        }     
    });
}
async function getGrados(idNivel){
    var gradosDelNivel= await new Promise((resolve, reject) => {
        $.getJSON(`/pruebaAJAXgrados/${idNivel}`, function (grados) {
            resolve(grados);
        });
    });
    return gradosDelNivel;
}
async function getMaterias(idGrado){
    var materiasDelGrado= await new Promise((resolve, reject) => {
        $.getJSON(`/pruebaAJAXmaterias/${idGrado}`, function (materias) {  
            resolve(materias);
        });
    });
    return materiasDelGrado;
}
async function getBloques(idMateria){
    var bloquesPorMateria= await new Promise((resolve, reject) => {
        $.getJSON(`/pruebaAJAXbloques/${idMateria}`, function (bloques) {
                resolve(bloques);
        });
    });
    return bloquesPorMateria;
}
async function getSecuencias(idBloque){
    var secuenciasPorBloque= await new Promise((resolve, reject) => {
        $.getJSON(`/pruebaAJAXsecuencias/${idBloque}`, function (secuencias) {
            resolve(secuencias);
        });
    });
    return secuenciasPorBloque;
}
/* Campos de Quiz */
$('#nivel').on('change', changeNivel);
async function changeNivel(){
    const val_actual = $('#nivel').val();
    document.getElementById("grado").disabled = true;
    document.getElementById("grado").innerHTML = "";
    document.getElementById("materia").disabled = true;
    document.getElementById("materia").innerHTML = "";
    document.getElementById("bloque").disabled = true;
    document.getElementById("bloque").innerHTML = "";
    document.getElementById("secuencia").disabled = true;
    document.getElementById("secuencia").innerHTML = "";
    if(val_actual != ''){
        var getChange= await getGrados(val_actual);
        document.getElementById("grado").innerHTML += "<option value=''></option>";
        for (var i = 0; i < getChange.length; i++) {
            document.getElementById("grado").innerHTML += "<option value='" + (getChange[i].id) + "'>" + (getChange[i].nombre) + "</option>";
        }
        document.getElementById("grado").disabled = false;
    }
}
$('#grado').on('change', changeGrado);
async function changeGrado(){
    const val_actual = $('#grado').val();
    document.getElementById("materia").disabled = true;
    document.getElementById("materia").innerHTML = "";
    document.getElementById("bloque").disabled = true;
    document.getElementById("bloque").innerHTML = "";
    document.getElementById("secuencia").disabled = true;
    document.getElementById("secuencia").innerHTML = "";
    if(val_actual != ''){
        var getChange = await getMaterias(val_actual);
        document.getElementById("materia").innerHTML += "<option value=''></option>";
        for (var i = 0; i < getChange.length; i++) {
            document.getElementById("materia").innerHTML += "<option value='" + (getChange[i].id) + "'>" + (getChange[i].nombre) + "</option>";
        }
        document.getElementById("materia").disabled = false;
    }

}
$('#materia').on('change', changeMateria);
async function changeMateria(){
    const val_actual = $('#materia').val();
    document.getElementById("bloque").disabled = true;
    document.getElementById("bloque").innerHTML = "";
    document.getElementById("secuencia").disabled = true;
    document.getElementById("secuencia").innerHTML = "";
    if(val_actual != ''){
        var getChange = await getBloques(val_actual);
        document.getElementById("bloque").innerHTML += "<option value=''></option>";
        for (var i = 0; i < getChange.length; i++) {
            document.getElementById("bloque").innerHTML += "<option value='" + (getChange[i].id) + "'>" + (getChange[i].nombre) + "</option>";
        }
        document.getElementById("bloque").disabled = false;
    }
}
$('#bloque').on('change', changeBloque);
async function changeBloque(){
    const val_actual = $('#bloque').val();
    document.getElementById("secuencia").disabled = true;
    document.getElementById("secuencia").innerHTML = "";
    if(val_actual != ''){
        var getChange = await getSecuencias(val_actual);
        document.getElementById("secuencia").innerHTML += "<option value=''></option>";
        for (var i = 0; i < getChange.length; i++) {
            document.getElementById("secuencia").innerHTML += "<option value='" + (getChange[i].id) + "'>" + (getChange[i].nombre) + "</option>";
        }
        document.getElementById("secuencia").disabled = false;
    }
}
/* MENU Seleccionar tipo de Quiz mostrar ocultar barra */
$('.selectQuiz .newQuizButton').on("click",function(){
    const $new_button = $(this).parent('.selectQuiz');
    if($new_button.hasClass('active')){
        $new_button.removeClass('active');
    }
    else{
        $new_button.addClass('active');
    }
});
/* ELIMINAR PREGUNTA */
$('.quizContainer').on('click','.delete-pregunta',function(){
    Swal.fire({
        title: '¿Estas seguro de que quieres eliminar la pregunta?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        denyButtonText: `No eliminar`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $(this).parents('.card').remove();
            enumerar();
            Swal.fire('¡Eliminada!', '', 'success');
        } else if (result.isDenied) {
            Swal.fire('No se eliminó la pregunta', '', 'info')
        }
    });
});



/*---------- Crear quiz del menu ----------*/
$('.selectQuiz').on('click','.quizz',function(){
    $(this).parents('.selectQuiz').addClass('active');
    goToScroll();
});
function goToScroll(){
    enumerar();
    $('.card.quizz:last .dropify').dropify({
        messages: {
            'default': 'Agregar archivo',
            'replace': 'Reemplazar archivo',
            'remove':  'Quitar archivo',
            'error':   'Ooops, algo salió mal.'
        }
    });
    $('.dropify-clear').click(function(e){
        e.preventDefault();
        alert('Remove Hit');
        
      });
    var $container = $('html');
    var $scrollV = $('.card.quizz:last');
    // $('.quizContainer').animate({ top: 10 },1000);​
    $container.animate({
        scrollTop: $scrollV.offset().top
    }, 1000);
}
$('#save-quiz, #edit-quiz').on('click',function(){
    /*---------- Valida campos con vacios ----------*/
    $(".quizContainer input").each(function() { 
        if($(this).val() == '' && $(this).attr('data-type') != 'imagen' && $(this).attr('data-type') != 'instrucciones' ) {
            $(this).addClass('border-danger');
        }
        else {
            $(this).removeClass('border-danger');
        }
    });
    /*---------- Valida select vacios ----------*/
    $(".quizContainer select").each(function( index ) { 
        if($(this).val() == '') {
            $(this).addClass('border-danger');
        }
        else {
            $(this).removeClass('border-danger');
        }
    });
    /*---------- Valida que se seleccione respuesta correcta en OM y TF ----------*/
    $('.pregunta_truefalse').each(function(){
        var idPOM = $(this).attr('data-id');
        if($(`.pregunta_truefalse[data-id=${idPOM}] input.border-success[data-type="respuesta"]`).length < 1){
            $(`.pregunta_truefalse[data-id=${idPOM}] input[data-type="respuesta"]`).addClass('border-danger');
            Swal.fire({
                icon: 'error',
                title: '¡Error al guardar!',
                text: 'Es seleccionar la respuesta correcta',
                confirmButtonText: 'Revisar'
            })
        }
        else {
            $(`.pregunta_truefalse[data-id=${idPOM}] input[data-type="respuesta"]`).removeClass('border-danger');
        }
    });
    /*---------- Valida si alguno de los anteriores los marco como danger ----------*/
    if($('input.border-danger').length >= 1){
        Swal.fire({
            icon: 'error',
            title: '¡Error al guardar!',
            text: 'Es necesario llenar todos los datos',
            confirmButtonText: 'Revisar'
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              var $container = $('html');
              var $scrollV = $('.border-danger:first');
              $container.animate({
                  scrollTop: $scrollV.offset().top - 50
              }, 1000);
            }
        });
    }
    else{
        if($('.card.quizz').length == 0){
            Swal.fire({
                icon: 'error',
                title: '¡Error al guardar!',
                text: 'Es necesario agregar por lo menos un Quiz'
            });
        }
        else{
            if( checkQuizGlobals() ){
                Swal.fire({
                    title: '¿Guardar nuevo Quiz?',
                    showDenyButton: true,
                    // showCancelButton: true,
                    confirmButtonText: 'Guradar',
                    denyButtonText: `No guardar`,
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('.loading').fadeIn(500);
                        $('.loading .msg-cargando').html(`Cargando ${$('.card.quizz').length} preguntas`);
                        createQuiz();
                    } else if (result.isDenied) {
                        Swal.fire('Quiz no creado', '', 'info')
                    }
                });
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: '¡Error al guardar!',
                    text: 'Verificar los campos correctamente'
                });
            }
        }
    }
});
/** Verificar los datos generales del quiz **/
function checkQuizGlobals(){
    var msg = '';
    var status = true;
    if($('#nivel').val() == '') {console.log("nivel"); status = false; msg += " <br>nivel"}
    if($('#grado').val() == '') {console.log("grado"); status = false; msg += " <br>grado"}
    if($('#materia').val() == '') {console.log("materia"); status = false; msg += " <br>materia"}
    if($('#bloque').val() == '') {console.log("bloque"); status = false; msg += " <br>bloque"}
    if($('#secuencia').val() == '') {console.log("secuencia"); status = false; msg += " <br>secuencia"}
    if($('#nombre').val() == '') {console.log("nombre"); status = false; msg += " <br>nombre"}
    if($('#intentos').val() == '' && Number.isInteger(parseInt($('#intentos').val())) ){console.log("intentos"); status = false; msg += " <br>nivel"}
    return {status:status,msg:msg};
}
async function saveNewImage(imgData){
    var getUrl = await getImgUrl();
    if(getUrl.status == 200){
        const newUrlImg = getUrl.url;
        return new Promise((resolve, reject) => { 
            $.ajax({
                type: "POST",
                url: newUrlImg+"/api/v1.0.0/ebe/img/quiz",
                data: imgData,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", "Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng");
                },
                success: function(response) {
                    console.log('saveImg',response);
                    /** Eliminar automaticamente nueva imagen **/
                    // deleteImage(response.images[0].id);        
                    resolve(response); 
                },
                error: function(jqXHR, textStatus, errorMessage) {
                    console.log('saveImg',);
                    resolve(errorMessage); 
                }
            });
        });
    }
}
async function deleteImage(imgId){
    var getUrl = await getImgUrl();
    if(getUrl.status == 200){
        const newUrlImg = getUrl.url;
        return new Promise((resolve, reject) => { 
            $.ajax({
                type: "POST",
                url: newUrlImg+"/api/v1.0.0/ebe/img/delete/quiz",
                data: {"photo_id":imgId,"public_key":"FCkU1oUxAwH2CCW9K3GfLYgEBVXJ0ga3"},
                dataType: 'json',
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", "Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng");
                },
                success: function(response) {
                    console.log('saveImg',response);
                    resolve(response); 
                },
                error: function(jqXHR, textStatus, errorMessage) {
                    console.log('saveImg',textStatus, errorMessage);
                    resolve(errorMessage); 
                }
            });
        });
    }
}
function getImgUrl(){
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "https://api.jenios.mx/jenios/v1.0.0/proyectos/bucket",
            type: "GET",
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", "Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng");
            },
            success: function(response) {
                console.log('getURL ok');
                resolve(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('getURL error');
                resolve(errorMessage);
            }
        });
    });
}
function enumerar(){
    $('.card.quizz').each(function(index){
        if($(this).find('.numerador').length == 0)
            $(this).find('.card-header').prepend(`<div class="numerador">${(index+1)}</div>`);
        else
            $(this).find('.numerador').html(index+1);;
    });
}