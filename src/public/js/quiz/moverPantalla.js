let contadorQuiz = 1;
let totalProgress = 0;
$('.arrowQuiz.nextQuiz').on(clickHandler, function(){
	nextQuiz();
});
function nextQuiz(){
	contadorQuiz++;
	totalProgress += progress;
	if(contadorQuiz == $(".quiz").length ){
		clearInterval(cronometroTime);
		$('.arrowQuiz.nextQuiz').css('display','none');
	}
	else{
		$('.textCount').text(contadorQuiz+'/'+quizArray.quizz.cuestionario.length);
	}
	$('.arrowQuiz').css('pointer-events','none');
	$('.progressBar').animate({
		width: totalProgress+'%'
	},500);
	$('.arrowQuiz.prevQuiz').css('display','block');
	$(".quiz[data-quiz="+(contadorQuiz-1)+"]").animate({
		left: '-100%'
	},500);
	$( ".quiz[data-quiz="+contadorQuiz+"]").animate({
		left: '0%'
	},500, function(){
		$('.arrowQuiz').css('pointer-events','visible');
	});
}
$('.arrowQuiz.prevQuiz').on(clickHandler, function(){
	prevQuiz();
});
function prevQuiz(){
	contadorQuiz--;
	totalProgress -= progress;
	if(contadorQuiz == $(".quiz").length-1 ){
		playTemporizador();
	}
	else{
		$('.textCount').text(contadorQuiz+'/'+quizArray.quizz.cuestionario.length);
	}
	$('.arrowQuiz').css('pointer-events','none');
	$('.arrowQuiz.nextQuiz').css('display','block');
	$('.progressBar').animate({
		width: totalProgress+'%'
	},500);
	$(".quiz[data-quiz="+(contadorQuiz+1)+"]").animate({
		left: '100%'
	},500);
	$( ".quiz[data-quiz="+contadorQuiz+"]").animate({
		left: '0%'
	},500, function(){
		$('.arrowQuiz').css('pointer-events','visible');
	});
	if(contadorQuiz == 1 ){
		$('.arrowQuiz.prevQuiz').css('display','none');
	}
}
