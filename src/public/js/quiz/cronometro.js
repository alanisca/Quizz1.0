let cronometroTime = 0;
let horas = 0;
let minutos = 0;
let segundos = 0;
function iniTemporizador(){
	horas = 0;
	minutos = 30;
	segundos = 0;
	$('.temporizador .tiempo.horas').html(horas+':').attr("data-init",horas);
	$('.temporizador .tiempo.minutos').html(minutos+':').attr("data-init",minutos);
	$('.temporizador .tiempo.segundos').html('0'+segundos).attr("data-init",segundos);
	playTemporizador();
}
function playTemporizador(){
	console.log('play');
	cronometroTime = setInterval(function(){
		segundos--;
		if(segundos <= 0){
			minutos--;
			segundos = 59;
			if(minutos < 10){
				$('.temporizador .tiempo.minutos').html('0'+minutos+':');
			}
			else{
				$('.temporizador .tiempo.minutos').html(minutos+':');
			}
			if(minutos <= 0){
				clearInterval(cronometroTime);
			}
		}
		if(segundos < 10){
			$('.temporizador .tiempo.segundos').html('0'+segundos);
		}
		else{
			$('.temporizador .tiempo.segundos').html(segundos);
		}
		let totalTiempo = $('.temporizador .tiempo.horas').text()+$('.temporizador .tiempo.minutos').text()+$('.temporizador .tiempo.segundos').text();
		$('.tiempoCalificacion').text(totalTiempo);
	},1000);
}