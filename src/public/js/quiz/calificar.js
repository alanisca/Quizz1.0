$(".containerQuiz").on("click",".botonCalificiacion",calificar);
function calificar(){
    const idAlumno = $(".mainContainer").attr("data-alumno");
    const id_quiz = $(".mainContainer").attr("data-id");
    const URL_Calificar = `/alumnos/correccion/alumno/${idAlumno}`;
    var pre_calif = 0;
    var calif_final = 0;
    var respuestas = [];

    for (let index = 1; index < $(".containerQuiz .quiz").length; index++) {
        var respuesta = [];
        var revision = [];
        var calif = 0;
        switch ($(`.containerQuiz .quiz[data-quiz=${index}]`).attr("data-type")) {
            case "tipoT":
                respuesta.push( $(`.quiz[data-quiz=${index}] input`).val() );
                revision.push( quizArray["quizz"]["cuestionario"][index-1].respuesta[0] == respuesta[0] );
                if(revision[0]){ 
                    calif = 100;
                    pre_calif += 100;
                }
                break;
            case "tipoR":
                for (let indexTR = 1; indexTR <= quizArray["quizz"]["cuestionario"][index-1].pregunta.length; indexTR++) {
                    var  indexOpcion = $(`.quiz[data-quiz=${index}] .cotainerOption.question[data-opcion="${indexTR}"]`).attr('data-respuesta');
                    // respuesta.push( $(`.quiz[data-quiz=${index}] .cotainerOption.answer[data-respuesta="${indexTR}"] .containerText`).html() );
                    // revision.push( $(`.quiz[data-quiz=${index}] .cotainerOption.answer[data-respuesta="${indexTR}"] .containerText`).html() == quizArray["quizz"]["cuestionario"][index-1].respuesta[indexTR-1] );
                    respuesta.push( $(`.quiz[data-quiz=${index}] .cotainerOption.answer[data-respuesta="${indexOpcion}"] .containerText`).html() );
                    revision.push( $(`.quiz[data-quiz=${index}] .cotainerOption.answer[data-respuesta="${indexOpcion}"] .containerText`).html() == quizArray["quizz"]["cuestionario"][index-1].respuesta[indexTR-1] );
                    console.log( revision[revision.length-1] );
                    if(revision[revision.length-1]) {
                        calif = (100/quizArray["quizz"]["cuestionario"][index-1].pregunta.length);
                        pre_calif += (100/quizArray["quizz"]["cuestionario"][index-1].pregunta.length);
                    }
                }
                break;
            case "tipoIT":
                respuesta.push( $(`.quiz[data-quiz=${index}] input`).val() );
                revision.push( quizArray["quizz"]["cuestionario"][index-1].respuesta[0] == respuesta[0] );
                if(revision[0]){ 
                    calif = 100;
                    pre_calif += 100;
                }
                break;
            case "tipoM":
                respuesta.push( $(`.quiz[data-quiz=${index}] input`).val() );
                revision.push( quizArray["quizz"]["cuestionario"][index-1].respuesta[0] == respuesta[0] );
                if(revision[0]){ 
                    calif = 100;
                    pre_calif += 100;
                }
                break;
            case "tipoOM":
                if($(`.quiz[data-quiz=${index}]`).hasClass("trueFalse")){
                    respuesta.push( $(`.quiz[data-quiz=${index}] .botonAnswer.select`).html() );
                    revision.push( quizArray["quizz"]["cuestionario"][index-1].respuesta[0] == respuesta[0]);
                }
                else{
                    respuesta.push( $(`.quiz[data-quiz=${index}] .containerRadius.select`).siblings('.containerText').html() );
                    revision.push( quizArray["quizz"]["cuestionario"][index-1].respuesta[0] == respuesta[0]);
                }
                if(revision[0]){ 
                    calif = 100;
                    pre_calif += 100;
                }
                break;
            case "tipoAP":
                for (let indexAP = 1; indexAP <= quizArray["quizz"]["cuestionario"][index-1].pregunta.length; indexAP++) {
                    var arrDrops = [];
                    var arrRevision = [];
                    $(`.quiz[data-quiz=${index}] .exercise[index="${indexAP}"] .containerTarget`).each(function(indexDrop){
                        arrDrops.push($(this).text());
                        arrRevision.push($(this).text() == quizArray["quizz"]["cuestionario"][index-1].respuesta[indexAP-1].correctas[indexDrop][0].resp);
                        if(arrRevision[arrRevision.length-1]) {
                            calif = (100/$(`.quiz[data-quiz=${index}] .containerTarget`).length);
                            pre_calif += (100/$(`.quiz[data-quiz=${index}] .containerTarget`).length);
                        }
                    });
                    respuesta.push( arrDrops );
                    revision.push( arrRevision );
                }
                break
            default:
                break;
        }
        console.log(index, calif, pre_calif);
        respuestas.push({"cuestionarioID": id_quiz,"respuestaA":respuesta,"revision":revision});
    }
    calif_final = pre_calif/($(".containerQuiz .quiz").length-1);
    const timepo_final = $('.temporizador .tiempo.horas').text()+$('.temporizador .tiempo.minutos').text()+$('.temporizador .tiempo.segundos').text();
    
    const params = {
        "alumno": idAlumno,
        "revisado": true,
        "quizz": id_quiz,
        "calificacion": calif_final,
        "tiempo": timepo_final,
        "respuestas": respuestas
    }
    console.log(params);
    $.redirect(URL_Calificar, params);
    // $.post(URL_Calificar, params, function(){
    // });
}


$(".containerQuiz").on("click",".botonModificar",function(){
    // window.location.href = "/editores/:token/editar/"+$(".mainContainer").attr("data-id");
    window.location.href = "/quiz/editQuizz/"+$(".mainContainer").attr("data-id");
});
$(".containerQuiz").on("click",".botonAprobar",function(){
    window.location.href = "/verify/token";
});