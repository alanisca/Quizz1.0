/*---------- PREGUNTA VERDADERO-FALSO ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-truefalse', function(){newPreguntaVF()});
function newPreguntaVF(arrQ = 1){
    var n_instruccion = '';
    var n_img = '';
    var n_img_id = '';
    var n_pregunta = '';
    var n_respuesta1 = 'VERDADERO';
    var n_respuesta2 = 'FALSO';
    var n_correcta = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_img = arrQ.imgRoute;
        n_img_id = arrQ.idImg;
        n_pregunta = arrQ.pregunta[0];
        n_respuesta1 = arrQ.pregunta[1];
        n_respuesta2 = arrQ.pregunta[2];
        n_correcta = arrQ.respuesta[0];
    }
    const pregunta = `<div class="card quizz pregunta_truefalse" data-quiz="pregunta_truefalse" data-id=${cont_quiz}>
                        <div class="card-header">
                            <h4 class="card-title">Verdadero o falso</h4>
                            <small><code>Instrucciones:</code>Define la pregunta al alumno y selecciona la opción correcta.</small>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- <div class="form-group col-12">
                                    <label>Instrucciones para el alumno:</label>
                                    <input type="text" class="form-control" data-type="instrucciones" placeholder="Escribir instrucciones" value="${n_instruccion}">
                                </div> -->
                                <div class="form-group col-12">                                
                                    <input type="file" class="dropify" data-type="imagen" data-height="70" data-max-file-size="1M" data-allowed-file-extensions="png jpeg jpg gif" img-id="${n_img_id}" data-default-file="${n_img}"/>
                                </div>
                                <div class="form-group col-12">
                                    <label>Pregunta:</label>
                                    <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta" value="${n_pregunta}">
                                </div>
                                <div class="opciones">
                                    <div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control ${(n_correcta == n_respuesta1 ? "border-success" : "")}" data-type="respuesta" placeholder="Escribir respuesta" value="${n_respuesta1}">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control ${(n_correcta == n_respuesta2 ? "border-success" : "")}" data-type="respuesta" placeholder="Escribir respuesta" value="${n_respuesta2}">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                        </div>
                    </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}
/*---------- PREGUNTA OPCION MULTIPLE ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-omultiple',function(){newPreguntaOMultiple()});
function newPreguntaOMultiple(arrQ = 1){
    var n_instruccion = '';
    var n_img = '';
    var n_img_id = '';
    var n_img_id = '';
    var n_pregunta = '';
    var n_correcta = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_img = arrQ.imgRoute;
        n_img_id = arrQ.idImg;
        n_pregunta = arrQ.pregunta[0];
        n_correcta = arrQ.respuesta[0];
    }
    var pregunta = `<div class="card quizz pregunta_truefalse" data-quiz="pregunta_truefalse" data-id=${cont_quiz}>
                        <div class="card-header">
                            <h4 class="card-title">Opcion multiple</h4>
                            <small><code>Instrucciones:</code>Para hacer una pregunta Verdadero-Falso , solo define la pregunta al alumno y selecciona la opción correcta. Si deseas opción Multiple,define la pregunta del alumno, borra los campos de verdadero y falso y escribe las respuestas que desees . Finalmente selecciona las correctas</small>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- <div class="form-group col-12">
                                    <label>Instrucciones para el alumno:</label>
                                    <input type="text" class="form-control" data-type="instrucciones" placeholder="Escribir instrucciones" value="${n_instruccion}">
                                </div> -->
                                <div class="form-group col-12">                              
                                    <input type="file" class="dropify" data-type="imagen" data-height="70" data-max-file-size="1M" data-allowed-file-extensions="png jpeg jpg gif" img-id="${n_img_id}" data-default-file="${n_img}"/>
                                </div>
                                <div class="form-group col-12">
                                    <label>Pregunta:</label>
                                    <input type="text" data-type="pregunta" class="form-control" placeholder="Escribir pregunta" value="${n_pregunta}">
                                </div>
                                <div class="opciones">`;
    if(arrQ == 1){//Inserta 2 preguntas y respuestas en blanco
        pregunta +=                 `<div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>`;
                                
    }
    else{
        for (let index = 1; index < arrQ.pregunta.length; index++) {
            pregunta +=             `<div class="form-group col-6">
                                        <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control ${(n_correcta == arrQ.pregunta[index] ? "border-success" : "")}" data-type="respuesta" placeholder="Escribir respuesta" value="${arrQ.pregunta[index]}">
                                            <div class="input-group-append seleccionarCorrecta">
                                                <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                            </div>
                                        </div>
                                    </div>`
        }
    }
        pregunta +=             `</div>
                                <button type="button" class="btn btn-primary col-12 addOpcion">Agregar respuesta</button>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                        </div>
                    </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}
/* Preguntas solo respuesta */
$('.quizContainer').on('click','.addOpcion',function(){
    const newPAQuestion =   `<div class="form-group col-6">
                                <label>Respuesta: <small class="seleccionar"><code>Seleccionar respuesta correcta</code></small></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta">
                                    <div class="input-group-append seleccionarCorrecta">
                                        <span class="input-group-text" id="basic-addon2"><span class="material-icons">done</span></span>
                                    </div>
                                    <div class="input-group-append rmv-opcion">
                                        <span class="input-group-text" id="basic-addon2"><span class="material-icons">delete</span></span>
                                    </div>
                                </div>
                            </div>`;
    $(this).siblings('.opciones').append(newPAQuestion);
});
/* Quitar solo pregunta OMULTIPLE */
$('.quizContainer').on('click','.rmv-opcion', function(){
    $(this).parents('.form-group').remove();
});
/* Seleccionar opción correcta */
$('.quizContainer').on('click','.seleccionarCorrecta.input-group-append',function(){
    var idQuizOM = $(this).parents('.card.quizz').attr("data-id");
    $(`.card.quizz[data-id="${idQuizOM}"] .opciones input`).removeClass('border-success');
    $(this).siblings('input').addClass('border-success').removeClass('border-danger');
});
/** CARGAR DATOS PARA GUARDAR **/
async function newQuizPreguntaTF(idQuiz){
    /** CARGA DE IMAGEN **/
    var imgId = '';
    var imgRoute = '';
    var reqImagen = false;
    var $imgVal = $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input[data-type="imagen"]`);
    if($imgVal.val() != ''){//tiene nueva imagen
        $imgVal.attr('img-id','');//Se quita el id de la imagen previa
        $imgVal.attr('data-default-file','');//Quita la imagen actual
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
        var blobFile = $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input[data-type="imagen"]`)[0].files[0];
        var formData = new FormData();
        formData.append("file", blobFile);
        var imgData = await saveNewImage(formData);
        imgId = imgData.images[0].id;
        imgRoute = imgData.images[0].src;
    }
    if($imgVal.attr('data-default-file') != '' && $imgVal.attr('data-default-file') != undefined && $imgVal.attr('img-id') != undefined && $imgVal.attr('img-id') != ''){
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
        imgId = $imgVal.attr('img-id');
        imgRoute = $imgVal.attr('data-default-file');
    }
    /** DATOS GENERALES **/
    var arr_newPA = {};
    var instrucciones = $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input[data-type="instrucciones"]`).val();
    arr_newPA.idImg = imgId;
    arr_newPA.tipo = "tipoOM";
    arr_newPA.img = reqImagen;
    arr_newPA.imgRoute = imgRoute;
    arr_newPA.instrucciones = instrucciones;
    /** Preguntas 1 y respuestas 2 y 3 **/
    var preguntas = [];
    preguntas.push($(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input[data-type="pregunta"]`).val());
    $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input[data-type="respuesta"]`).each(function(index){
        preguntas.push($(this).val());
    });

    var respuestas = $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input.border-success[data-type="respuesta"]`).val();
    arr_newPA.pregunta = preguntas;
    arr_newPA.respuesta = respuestas;
    console.log("Arreglo",arr_newPA);
    return new Promise((resolve, reject) => { 
        resolve(arr_newPA);
    });
}