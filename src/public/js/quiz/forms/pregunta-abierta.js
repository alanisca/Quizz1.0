/*---------- PREGUNTA ABIERTA ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-abierta',function(){newPreguntaAbierta()});
function newPreguntaAbierta(arrQ = 1){
    var n_instruccion = '';
    var n_img = '';
    var n_img_id = '';
    var n_pregunta = '';
    var n_respuesta = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_img = arrQ.imgRoute;
        n_img_id = arrQ.idImg;
        n_pregunta = arrQ.pregunta[0];
        n_respuesta = arrQ.respuesta[0];
    }
    const pregunta = `<div class="card quizz pregunta_abierta" data-quiz="pregunta_abierta" data-id=${cont_quiz}>
                                <div class="card-header">
                                    <h4 class="card-title">Pregunta abierta</h4>
                                    <small><code>Instrucciones:</code> Defina pregunta y respuesta, opcionalmente se puede agregar una imagen de apoyo. </small>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- <div class="form-group col-12">
                                            <label>Instrucciones para el alumno:</label>
                                            <input type="text" class="form-control" data-type="instrucciones" placeholder="Escribir instrucciones" value="${n_instruccion}">
                                        </div> -->
                                        <div class="form-group col-12">
                                            <input type="file" class="dropify" data-type="imagen" data-height="70" data-max-file-size="1M" data-allowed-file-extensions="png jpeg jpg gif" img-id="${n_img_id}" data-default-file="${n_img}"/>
                                        </div>
                                        <div class="opciones">
                                            <div class="form-group col-12">
                                                <label>Pregunta:</label>
                                                <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta" value="${n_pregunta}">
                                            </div>
                                            <div class="form-group col-12">
                                                <label>Respuesta:</label>
                                                <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta" value="${n_respuesta}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                                </div>
                            </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}

/** CARGAR DATOS PARA GUARDAR **/
async function newQuizPreguntaAbierta(idQuiz){
    /** CARGA DE IMAGEN **/
    var imgId = '';
    var imgRoute = '';
    var reqImagen = false;
    var $imgVal = $(`.quizz.pregunta_abierta[data-id=${idQuiz}] input[data-type="imagen"]`);
    if($imgVal.val() != ''){//tiene nueva imagen
        $imgVal.attr('img-id','');//Se quita el id de la imagen previa
        $imgVal.attr('data-default-file','');//Quita la imagen actual
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
        var blobFile = $(`.quizz.pregunta_abierta[data-id=${idQuiz}] input[data-type="imagen"]`)[0].files[0];
        var formData = new FormData();
        formData.append("file", blobFile);
        var imgData = await saveNewImage(formData);
        console.log(imgData);
        imgId = imgData.images[0].id;
        imgRoute = imgData.images[0].src;
    }
    if($imgVal.attr('data-default-file') != '' && $imgVal.attr('data-default-file') != undefined && $imgVal.attr('img-id') != undefined && $imgVal.attr('img-id') != ''){
        imgId = $imgVal.attr('img-id');
        imgRoute = $imgVal.attr('data-default-file');
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
    }
    /** DATOS GENERALES **/
    var arr_newPA = {};
    var instrucciones = $(`.quizz.pregunta_abierta[data-id=${idQuiz}] input[data-type="instrucciones"]`).val();
    arr_newPA.idImg = imgId;
    arr_newPA.tipo = "tipoT";
    arr_newPA.img = reqImagen;
    arr_newPA.imgRoute = imgRoute;
    arr_newPA.instrucciones = instrucciones;
    /** Preguntas y respuestas **/
    var preguntas = $(`.quizz.pregunta_abierta[data-id=${idQuiz}] input[data-type="pregunta"]`).val();
    var respuestas = $(`.quizz.pregunta_abierta[data-id=${idQuiz}] input[data-type="respuesta"]`).val();
    arr_newPA.pregunta = preguntas;
    arr_newPA.respuesta = respuestas;
    console.log("Arreglo",arr_newPA);
    return new Promise((resolve, reject) => { 
        console.log('saveQuizPA');
        resolve(arr_newPA);
    });
}