/*---------- PREGUNTA ARRASTRAR PALABRAS ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-arrastrarP',function(){newArrastrarPalabra()});
function newArrastrarPalabra(arrQ = 1){
    var n_instruccion = '';
    var n_img = '';
    var n_img_id = '';
    var n_img_id = '';
    var n_pregunta = '';
    var n_correcta = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_img = arrQ.imgRoute;
        n_img_id = arrQ.idImg;
        n_pregunta = arrQ.pregunta[0];
        n_correcta = arrQ.respuesta[0];
    }
    var pregunta = `<div class="card quizz arrastrar_palabras" data-quiz="pregunta_arrastrar_palabras" data-id="${cont_quiz}">
                        <div class="card-header">
                            <h4 class="card-title">Arrastrar palabras</h4>
                            <small><code>Instrucciones:</code> Defina pregunta y agregue _ donde se arrastrarán las respuestas , opcionalmente se puede agregar una imagen de apoyo. </small>
                        </div> 
                        <div class="card-body">
                            <div class="row">
                                <!-- <div class="form-group col-12">
                                    <label>Instrucciones para el alumno:</label>
                                    <input type="text" class="form-control" data-type="instrucciones" placeholder="Escribir instrucciones" value="${n_instruccion}">
                                </div> -->
                                <div class="form-group col-12">
                                    <input type="file" class="dropify" data-type="imagen" data-height="70" data-max-file-size="1M" data-allowed-file-extensions="png jpeg jpg gif" img-id="${n_img_id}" data-default-file="${n_img}"/>
                                </div>
                                <div class="opciones">`;
    if(arrQ == 1){//Nuevo quiz arrastrar
        pregunta += `<div class="respuestas">${crearPregunta()}</div>`;
    }
    else{//Editar quiz arrastrar
        for (let index = 0; index < arrQ.pregunta.length; index++) {
            pregunta+=          `<div class="respuestas"> ${crearPregunta(arrQ.pregunta[index], (index+1))}`;
            if(arrQ.respuesta[index].correctas)
                for (let index2 = 0; index2 < arrQ.respuesta[index].correctas.length; index2++) {
                        pregunta += crearRespuestaCorrecta(arrQ.respuesta[index].correctas[index2][0].resp);
                }
            if(arrQ.respuesta[index].incorrectas)
                for (let index2 = 0; index2 < arrQ.respuesta[index].incorrectas.length; index2++) {
                    pregunta+=          crearRespuestaIncorrecta(arrQ.respuesta[index].incorrectas[index2]);
        }
            pregunta+=              `</div>`;
        }
    }
    pregunta+=                 `</div>
                                <button type="button" class="btn btn-primary col-12 addArrastrar">Agregar pregunta</button>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                        </div>
                    </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}
/*---------- Agregar pregunta en arrastrar ----------*/
$('.quizContainer').on('click','.arrastrar_palabras .addArrastrar',function(){
    const nuevaPregunta =  `<div class="respuestas">
                                ${crearPregunta( '', $(this).siblings('.opciones').find(".respuestas").length+1 )}
                            </div>`
    $(this).siblings('.opciones').append(nuevaPregunta);
});
function crearPregunta(pregunta = '', numeracion = 1){
    typeOpc =  `
                    <div class="form-group col-12">
                        <label>Pregunta:</label>
                        <small> Escribe tu oración y coloca guión bajo <code>_</code> en donde se quiera colocar la palabra a arrastrar. </small>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text num-arrastrar">${numeracion}</span>
                            </div>
                            <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta _ para completar" value="${pregunta}">
                            <div class="input-group-append">
                                ${(numeracion == 1 ? '' : `<span class="input-group-text rmv-pregunta" ><span class="material-icons">delete</span></span>`)}
                                <span class="input-group-text add-arrastrar-incorrecta"><span class="material-icons">add_box</span></span>
                                <span class="input-group-text add-arrastrar"><span class="material-icons">login</span></span>
                            </div>
                        </div>
                    </div>`;
    return typeOpc;
}
/*---------- Agregar respuesta incorrecta ----------*/
$('.quizContainer').on('click','.arrastrar_palabras .add-arrastrar-incorrecta',function(){
    $(this).parents('.respuestas').append(crearRespuestaIncorrecta());
});
function crearRespuestaIncorrecta(respuesta = ''){
    typeOpc =   `<div class="form-group col-6 respuesta <code>incorrecta</code>">
                    <label>Palabra arrastrable <code>incorrecta</code>:</label>
                    <div class="input-group">
                        <input type="text" class="form-control incorrect" data-type="respuesta" placeholder="Escribir incorrecto" value="${respuesta}">
                        <div class="input-group-append">
                            <span class="input-group-text rmv-opc"><span class="material-icons">delete</span></span>
                        </div>
                    </div>
                </div>`;
    return typeOpc;
}
/*---------- Agregar respuesta por cada _ en el input de pregunta ----------*/
$('.quizContainer').on('keypress','.arrastrar_palabras input[data-type="pregunta"]',function(e) {
    if(e.which == 13) {
        crearRespuestas($(this));
    }
});
$('.quizContainer').on('click','.arrastrar_palabras .add-arrastrar',function(){
    crearRespuestas($(this).parent().prev('input'));
});
function crearRespuestas($pregunta){
    const pregunta = $pregunta.val();
    var cont_resp = 0;
    var typeOpc = ``;
    // $pregunta.parents('.respuestas').children('.respuesta').remove();
    const tot_resp = $pregunta.parents('.respuestas').children('.respuesta.correcta').length;
    for(var i = 0; i < pregunta.length; i++) {
        if (pregunta[i] === "_" ) {
            cont_resp++;
            if(tot_resp < cont_resp)
            typeOpc +=   crearRespuestaCorrecta();
        }
    }
    if(cont_resp == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Datos necesarios',
            text: 'Es necesario agregar texto y _ en el lugar donde se colocarán las respuestas correctas.',
        });
        return;
    }
    $pregunta.parents('.respuestas').append(typeOpc);
}

function crearRespuestaCorrecta(arrastable = ""){
    typeOpc =   `<div class="form-group col-6 respuesta correcta">
                            <label>Palabra arrastrable correcta:</label>
                            <div class="input-group">
                                <input type="text" class="form-control correct" data-type="respuesta" placeholder="Escribir correcto" value="${arrastable}">
                                <div class="input-group-append">
                                    <!-- <span class="input-group-text agregarOpcion"><span class="material-icons">done_all</span></span> -->
                                    <span class="input-group-text rmv-opc"><span class="material-icons">delete</span></span>
                                </div>
                            </div>
                        </div>`;
    return typeOpc;
}
/* Quitar solo pregunta OMULTIPLE */
$('.quizContainer').on('click','.arrastrar_palabras .rmv-pregunta', function(){
    $(this).parents('.respuestas').remove();
});
/*---------- Agregar opcion multiple en arrastrar ----------*/
$('.quizContainer').on('click','.input-group-append .agregarOpcion',function(){
    $(this).parents('.form-group').append(`<div class="input-group multiple">
                                                <input type="text" class="form-control " data-type="respuesta" placeholder="Escribir">
                                                <div class="input-group-append rmv-multiple">
                                                    <span class="input-group-text"><span class="material-icons">delete</span></span>
                                                </div>
                                            </div>`);
});
/* Quitar solo palabra arrastrar */
$('.quizContainer').on('click','.rmv-opc', function(){
    $(this).parents('.respuesta').remove();
});
/* Quitar solo palabra adicional */
$('.quizContainer').on('click','.rmv-multiple', function(){
    $(this).parents('.multiple').remove();
});
/** CARGAR DATOS PARA GUARDAR **/
async function newQuizArrastrarP(idQuiz){
    /** CARGA DE IMAGEN **/
    var imgId = '';
    var imgRoute = '';
    var reqImagen = false;
    var $imgVal = $(`.quizz.arrastrar_palabras[data-id=${idQuiz}] input[data-type="imagen"]`);
    if($imgVal.val() != ''){//tiene nueva imagen
        $imgVal.attr('img-id','');//Se quita el id de la imagen previa
        $imgVal.attr('data-default-file','');//Quita la imagen actual
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'),$imgVal.val());
        var blobFile = $(`.quizz.arrastrar_palabras[data-id=${idQuiz}] input[data-type="imagen"]`)[0].files[0];
        var formData = new FormData();
        formData.append("file", blobFile);
        var imgData = await saveNewImage(formData);
        imgId = imgData.images[0].id;
        imgRoute = imgData.images[0].src;
    }
    if($imgVal.attr('data-default-file') != '' && $imgVal.attr('data-default-file') != undefined && $imgVal.attr('img-id') != undefined && $imgVal.attr('img-id') != ''){
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
        imgId = $imgVal.attr('img-id');
        imgRoute = $imgVal.attr('data-default-file');
    }
    /** DATOS GENERALES **/
    var arr_newPA = {};
    var instrucciones = $(`.quizz.arrastrar_palabras[data-id=${idQuiz}] input[data-type="instrucciones"]`).val();
    arr_newPA.idImg = imgId;
    arr_newPA.tipo = "tipoAP";
    arr_newPA.img = reqImagen;
    arr_newPA.imgRoute = imgRoute;
    arr_newPA.instrucciones = instrucciones;
    /** Preguntas 1 y respuestas 2 y 3 **/
    var pregunta = [];
    var respuesta = [];
    var correctas = [];
    var incorrectas = [];
    $(`.quizz.arrastrar_palabras[data-id=${idQuiz}] .respuestas`).each(function(index){
        // preguntas.push($(this).val());
        var $preg = $(this);
        pregunta.push($preg.find('input[data-type="pregunta"]').val());
        correctas = [];
        incorrectas = [];
        $preg.find('.respuesta input[data-type="respuesta"]').each(function(){
            if($(this).hasClass("correct"))
            correctas.push([ { resp:$(this).val(),tipo:"correcta" } ]);
            if($(this).hasClass("incorrect"))
            incorrectas.push([$(this).val()]);
        });
        respuesta.push({correctas:correctas,incorrectas:incorrectas});
    });
    console.log(pregunta);
    console.log(respuesta);
    $(`.quizz.pregunta_truefalse[data-id=${idQuiz}] input.border-success[data-type="respuesta"]`).val();
    arr_newPA.pregunta = pregunta;
    arr_newPA.respuesta = respuesta;
    console.log("Arreglo",arr_newPA);
    return new Promise((resolve, reject) => { 
        resolve(arr_newPA);
    });
}