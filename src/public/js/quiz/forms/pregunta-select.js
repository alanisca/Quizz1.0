/*---------- PREGUNTA seleccionar PALABRAS ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-seleccionar',function(){newPreguntaSeleccionar()});
function newPreguntaSeleccionar(arrQ = 1){
    // alert("En construcción");
    // return false;
    var n_instruccion = '';
    var n_img = '';
    var n_img_id = '';
    var n_img_id = '';
    var n_pregunta = '';
    var n_correcta = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_img = arrQ.imgRoute;
        n_img_id = arrQ.idImg;
        n_pregunta = arrQ.pregunta[0];
        n_correcta = arrQ.respuesta[0];
    }
    var pregunta = `<div class="card quizz preguntaSeleccionar" data-quiz="pregunta_preguntaSeleccionar" data-id="${cont_quiz}">
                        <div class="card-header">
                            <h4 class="card-title">Seleccionar respuesta para completar</h4>
                            <small><code>Instrucciones:</code> Defina pregunta y agregue _ donde se seleccionará la respuesta, opcionalmente se puede agregar una imagen de apoyo. </small>
                        </div> 
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-4" style="height: 80px !important;">
                                    <label>Imagen de apoyo:</label>
                                    <input type="file" class="dropify" data-type="imagen" data-height="70" data-max-file-size="1M" data-allowed-file-extensions="png jpeg jpg gif" img-id="${n_img_id}" data-default-file="${n_img}"/>
                                </div>
                                <div class="form-group col-8">
                                    <label>Texto para el ejercicio:</label>
                                    <textarea class="form-control" data-type="instrucciones" rows="3" placeholder="Escribir texto">${n_instruccion}</textarea>
                                </div>
                                <div class="opciones">`;
    if(arrQ == 1){//Nuevo quiz seleccionar
        pregunta += `<div class="respuestas">${crearPreguntaSeleccionar()}</div>`;
    }
    else{//Editar quiz seleccionar
        for (let index = 0; index < arrQ.pregunta.length; index++) {
            pregunta+=          `<div class="respuestas"> ${crearPreguntaSeleccionar(arrQ.pregunta[index], (index+1))}`;
            if(arrQ.respuesta[index].correctas)
                for (let index2 = 0; index2 < arrQ.respuesta[index].correctas.length; index2++) {
                        pregunta += crearRespuestaCorrectaSeleccionar(arrQ.respuesta[index].correctas[index2][0].resp, arrQ.respuesta[index].incorrectas[index2]);
                }
            pregunta+=           `</div>`;
        }
    }
    pregunta+=                 `</div>
                                <button type="button" class="btn btn-primary col-12 addSeleccionar">Agregar pregunta</button>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                        </div>
                    </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}
/*---------- Agregar pregunta en seleccionar ----------*/
$('.quizContainer').on('click','.preguntaSeleccionar .addSeleccionar',function(){
    const nuevaPregunta =  `<div class="respuestas">
                                ${crearPreguntaSeleccionar( '', $(this).siblings('.opciones').find(".respuestas").length+1 )}
                            </div>`
    $(this).siblings('.opciones').append(nuevaPregunta);
});
function crearPreguntaSeleccionar(pregunta = '', numeracion = 1){
    typeOpc =  `
                    <div class="form-group col-12">
                        <label>Pregunta:</label>
                        <small> Escribe tu oración y coloca guión bajo <code>_</code> en donde se quiera colocar las palabras a seleccionar. </small>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text num-seleccionar">${numeracion}</span>
                            </div>
                            <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta _ para seleccionar" value="${pregunta}">
                            <div class="input-group-append">
                                ${(numeracion == 1 ? '' : `<span class="input-group-text rmv-pregunta" ><span class="material-icons">delete</span></span>`)}
                                <!-- <span class="input-group-text add-seleccionar-incorrecta"><span class="material-icons">add_box</span></span> -->
                                <span class="input-group-text add-seleccionar"><span class="material-icons">login</span></span>
                            </div>
                        </div>
                    </div>`;
    return typeOpc;
}
/*---------- Agregar respuesta por cada _ en el input de pregunta ----------*/
$('.quizContainer').on('keypress','.preguntaSeleccionar input[data-type="pregunta"]',function(e) {
    if(e.which == 13) {
        crearRespuestasSeleccionar($(this));
    }
});
$('.quizContainer').on('click','.preguntaSeleccionar .add-seleccionar',function(){
    crearRespuestasSeleccionar($(this).parent().prev('input'));
});
function crearRespuestasSeleccionar($pregunta){
    const pregunta = $pregunta.val();
    var cont_resp = 0;
    var typeOpc = ``;
    // $pregunta.parents('.respuestas').children('.respuesta').remove();
    const tot_resp = $pregunta.parents('.respuestas').children('.respuesta.correcta').length;
    for(var i = 0; i < pregunta.length; i++) {
        if (pregunta[i] === "_" ) {
            cont_resp++;
            if(tot_resp < cont_resp)
                typeOpc +=   crearRespuestaCorrectaSeleccionar();
        }
    }
    if(tot_resp > cont_resp)
        for (let i = tot_resp; i > cont_resp; i--) {
            $pregunta.parents('.respuestas').children('.respuesta.correcta').last().remove();//Eliminar campos sobrantes
        }
    if(cont_resp == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Datos necesarios',
            text: 'Es necesario agregar texto y _ en el lugar donde se colocarán las respuestas.',
        });
        return;
    }
    $pregunta.parents('.respuestas').append(typeOpc);
}

function crearRespuestaCorrectaSeleccionar(arrastable = "", arrIncorrectas = []){
    typeOpc =   `<div class="form-group col-6 respuesta correcta">
                    <label>Opciones a seleccionar:</label>
                    <div class="input-group">
                        <input type="text" class="form-control correct" data-type="respuesta" placeholder="Escribir opción correcta" value="${arrastable}">
                        <div class="input-group-append">
                            <span class="input-group-text add-seleccionar-agregarOpcion"><span class="material-icons">add_box</span></span>
                        </div>
                    </div>`;                
    if(arrIncorrectas.length > 0)
        for (let index = 0; index < arrIncorrectas.length; index++) {
            typeOpc += creatRespuestaIncorrectaSeleccionar(arrIncorrectas[index].resp);
        }
    else
        typeOpc += creatRespuestaIncorrectaSeleccionar();
    typeOpc +=  `</div>`;
    return typeOpc;
}
/* Quitar solo pregunta OMULTIPLE */
$('.quizContainer').on('click','.preguntaSeleccionar .rmv-pregunta', function(){
    $(this).parents('.respuestas').remove();
});
/*---------- Agregar opcion multiple en seleccionar ----------*/
$('.quizContainer').on('click','.input-group-append .add-seleccionar-agregarOpcion',function(){
    $(this).parents('.form-group').append(creatRespuestaIncorrectaSeleccionar());
});
function creatRespuestaIncorrectaSeleccionar(respuesta = ''){
    const incorrectInput = `<div class="input-group multiple">
                                <input type="text" class="form-control " data-type="respuesta" placeholder="Escribir opción incorrecta" value="${respuesta}">
                                <div class="input-group-append rmv-multiple">
                                    <span class="input-group-text"><span class="material-icons">delete</span></span>
                                </div>
                            </div>`;
    return incorrectInput;
}
/* Quitar solo palabra seleccionar */
// $('.quizContainer').on('click','.rmv-opc', function(){
//     $(this).parents('.respuesta').remove();
// });
/* Quitar solo palabra adicional */
// $('.quizContainer').on('click','.rmv-multiple', function(){
//     $(this).parents('.multiple').remove();
// });
/** CARGAR DATOS PARA GUARDAR **/
async function newQuizseleccionarP(idQuiz){
    /** CARGA DE IMAGEN **/
    var imgId = '';
    var imgRoute = '';
    var reqImagen = false;
    var $imgVal = $(`.quizz.preguntaSeleccionar[data-id=${idQuiz}] input[data-type="imagen"]`);
    if($imgVal.val() != ''){//tiene nueva imagen
        $imgVal.attr('img-id','');//Se quita el id de la imagen previa
        $imgVal.attr('data-default-file','');//Quita la imagen actual
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'),$imgVal.val());
        var blobFile = $(`.quizz.pregunta-Seleccionar[data-id=${idQuiz}] input[data-type="imagen"]`)[0].files[0];
        var formData = new FormData();
        formData.append("file", blobFile);
        var imgData = await saveNewImage(formData);
        imgId = imgData.images[0].id;
        imgRoute = imgData.images[0].src;
    }
    if($imgVal.attr('data-default-file') != '' && $imgVal.attr('data-default-file') != undefined && $imgVal.attr('img-id') != undefined && $imgVal.attr('img-id') != ''){
        reqImagen = true;
        console.log("true",$imgVal.attr('data-default-file'));
        imgId = $imgVal.attr('img-id');
        imgRoute = $imgVal.attr('data-default-file');
    }
    /** DATOS GENERALES **/
    var arr_newPA = {};
    var instrucciones = $(`.quizz.preguntaSeleccionar[data-id=${idQuiz}] textarea[data-type="instrucciones"]`).val();
    arr_newPA.idImg = imgId;
    arr_newPA.tipo = "tipoSP";
    arr_newPA.img = reqImagen;
    arr_newPA.imgRoute = imgRoute;
    arr_newPA.instrucciones = instrucciones;
    /** Preguntas 1 y respuestas 2 y 3 **/
    var pregunta = [];
    var respuesta = [];
    var correctas = [];
    var incorrectas = [];
    $(`.quizz.preguntaSeleccionar[data-id=${idQuiz}] .respuestas`).each(function(){
        // preguntas.push($(this).val());
        var $preg = $(this);
        pregunta.push($preg.find('input[data-type="pregunta"]').val());
        $preg.find('.respuesta').each(function(indexPregunta){
            correctas[indexPregunta] = [];
            incorrectas[indexPregunta] = [];
            $(this).find('input[data-type="respuesta"]').each(function(indexRespuestas){
                console.log($(this).val());
                if($(this).hasClass("correct"))
                    correctas[indexPregunta].push({ resp:$(this).val(),tipo:"correcta" });
                else
                    incorrectas[indexPregunta].push({ resp:$(this).val(),tipo:"incorrecta" });
            });
        });
        // $preg.find('.respuesta input[data-type="respuesta"]').each(function(){
        // });
        respuesta.push({correctas:correctas,incorrectas:incorrectas});
    });
    arr_newPA.pregunta = pregunta;
    arr_newPA.respuesta = respuesta;
    console.log("Arreglo",arr_newPA);
    return new Promise((resolve, reject) => { 
        resolve(arr_newPA);
    });
}