/*---------- PREGUNTA RELACIONAL ----------*/
$(".selectQuiz").on('click','.quizz#pregunta-relacional',function(){newPreguntaRelacional()});
function newPreguntaRelacional(arrQ = 1){
    var n_instruccion = '';
    var n_img = '';
    var n_pregunta1 = '';
    var n_pregunta2 = '';
    
    var n_respuesta1 = '';
    var n_respuesta2 = '';
    if(arrQ != 1){
        n_instruccion = arrQ.instrucciones;
        n_pregunta1 = arrQ.pregunta[0];
        n_pregunta2 = arrQ.pregunta[1];
        n_respuesta1 = arrQ.respuesta[0];
        n_respuesta2 = arrQ.respuesta[1];
    }
    var pregunta = `<div class="card quizz pregunta_relacional" data-quiz="pregunta_relacional" data-id=${cont_quiz}>
                                <div class="card-header">
                                    <h4 class="card-title">Pregunta relacional</h4>
                                    <small><code>Instrucciones:</code>Se debe colocar la respuesta y pregunta en orden 1 a 1, para que pueda evaluar correctamente.No olvides definir las instrucciones para el Alumno</small>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- <div class="form-group col-12">
                                            <label>Instrucciones para el alumno:</label>
                                            <input type="text" class="form-control"  data-type="instrucciones" placeholder="Escribir instrucciones" value="${n_instruccion}">
                                        </div> -->
                                        <div class="opciones">
                                            <div class="opcion">
                                                <div class="form-group col-6">
                                                    <label>Pregunta:</label>
                                                    <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta" value="${n_pregunta1}">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label>Respuesta:</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta" value="${n_respuesta1}">
                                                    </div>
                                                </div>
                                                </div>
                                            <div class="opcion">
                                                <div class="form-group col-6">
                                                    <label>Pregunta:</label>
                                                    <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta" value="${n_pregunta2}">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label>Respuesta:</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta" value="${n_respuesta2}">
                                                    </div>
                                                </div>
                                            </div>`
        if(arrQ != 1){//Inserta las preguntas y respuestas para editar el quiz
            for (let index = 2; index < arrQ.pregunta.length; index++) {
                pregunta +=                 `<div class="opcion">
                                                <div class="form-group col-6">
                                                    <label>Pregunta:</label>
                                                    <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta" value="${arrQ.pregunta[index]}">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label>Respuesta:</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta" value="${arrQ.respuesta[index]}">
                                                        <div class="input-group-append rmv-opciones">
                                                            <span class="input-group-text" id="basic-addon2"><span class="material-icons">delete</span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`
            }
        }             
            pregunta +=             `</div>
                                        <button type="button" class="btn btn-primary col-12 addOpciones">Agregar reactvo</button>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="button" class="btn btn-danger delete-pregunta">Eliminar Pregunta</button>
                                </div>
                            </div>`;
    $('.quizContainer').append(pregunta);
    cont_quiz++;
}

/* Agregar Pregunta y Respuesta Relacional*/
$('.quizContainer').on('click','.addOpciones',function(){
    const newPAQuestion =   `<div class="opcion">
                                <div class="form-group col-6">
                                    <label>Pregunta:</label>
                                    <input type="text" class="form-control" data-type="pregunta" placeholder="Escribir pregunta">
                                </div>
                                <div class="form-group col-6">
                                    <label>Respuesta:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" data-type="respuesta" placeholder="Escribir respuesta">
                                        <div class="input-group-append rmv-opciones">
                                            <span class="input-group-text" id="basic-addon2"><span class="material-icons">delete</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
    $(this).siblings('.opciones').append(newPAQuestion);
});
/* Quitar respuesta con pregunta */
$('.quizContainer').on('click','.rmv-opciones', function(){
    $(this).parents('.opcion').remove();
});
/** CARGAR DATOS PARA GUARDAR **/
async function newQuizPreguntaRelacional(idQuiz){
    /** DATOS GENERALES **/
    var arr_newPA = {};
    var instrucciones = $(`.quizz.pregunta_relacional[data-id=${idQuiz}] input[data-type="instrucciones"]`).val();
    var reqImagen = false;
    arr_newPA.idImg = '';
    arr_newPA.tipo = 'tipoR';
    arr_newPA.img = reqImagen;
    arr_newPA.imgRoute = '';
    arr_newPA.instrucciones = instrucciones;
    /** Preguntas y respuestas **/
    var preguntas = [];
    var respuestas = [];
    $(`.quizz.pregunta_relacional[data-id=${idQuiz}] .opciones input`).each(function(){
        if($(this).attr('data-type') == 'pregunta') preguntas.push($(this).val());
        if($(this).attr('data-type') == 'respuesta') respuestas.push($(this).val());
    });
    arr_newPA.pregunta = preguntas;
    arr_newPA.respuesta = respuestas;
    console.log("Arreglo",arr_newPA);
    return new Promise((resolve, reject) => { 
        resolve(arr_newPA);
    });
}