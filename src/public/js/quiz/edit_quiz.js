var id_img = [];
async function getQuiz(idQuiz){
    $.ajax({
        type: 'GET',
        url: '/quiz/obtener/'+idQuiz,
		dataType: 'html',
        success: async function (data) {
            /* Validar que no ha sido contestado anteriormente */
            quizArray = JSON.parse(data);
            console.log('cargando datos generales1');
            while ($('#nivel').val() == "" ) {
                console.log('cargando datos generales');
                await loadGeneralData(quizArray);
            }
            console.log(quizArray);
            $('.loading').fadeOut(1000);
            const cuestionarios = quizArray.quizz.cuestionario;
            for (let index = 0; index < cuestionarios.length; index++) {
                if(cuestionarios[index].img) id_img.push(cuestionarios[index].idImg);
                switch (cuestionarios[index].tipo) {
                    case "tipoT":
                        newPreguntaAbierta(cuestionarios[index]);
                        break;
                    case "tipoR":
                        newPreguntaRelacional(cuestionarios[index]);
                        break;
                    case "tipoOM":
                        if(cuestionarios[index].pregunta.length > 3) newPreguntaOMultiple(cuestionarios[index]);
                        else newPreguntaVF(cuestionarios[index]);
                        break;
                    case "tipoAP":
                        newArrastrarPalabra(cuestionarios[index]);
                        break;
                    case "tipoSP":
                        newPreguntaSeleccionar(cuestionarios[index]);
                        break;
                    default:
                        break;
                }
            }
            enumerar();
            $('.card.quizz .dropify').dropify({
                messages: {
                    'default': 'Agregar archivo',
                    'replace': 'Reemplazar archivo',
                    'remove':  'Quitar archivo',
                    'error':   'Ooops, algo salió mal.'
                }
            });
            $('.dropify-clear').click(function(e){
                e.preventDefault();
                $(this).siblings('input.dropify').addClass('img_deleted');
                $(this).siblings('input.dropify').removeAttr('img-id');//Se quita el id de la imagen previa
                $(this).siblings('input.dropify').removeAttr('data-default-file');//Quita la imagen actual
                console.log("Eliminar imagen previa");
            });
        }
    });
}
async function loadGeneralData(quizArray){
    $('#nivel').val(quizArray.quizz.nivel);
    await changeNivel();
    $('#grado').val(quizArray.quizz.grado);
    await changeGrado();
    $('#materia').val(quizArray.quizz.materia);
    await changeMateria();
    $('#bloque').val(quizArray.quizz.bloque);
    await changeBloque();
    $('#secuencia').val(quizArray.quizz.secuencia);
    $('#nombre').val(quizArray.quizz.nombreQuizz);
    $('#intentos').val(quizArray.quizz.intentos);
}