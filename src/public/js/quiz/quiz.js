let clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
let contentQuiz = '';
let progress = 0;
var contadorSeleccion = 0;
// var numOpcion = 0;
// var dataOption = 0;
// var optQuestion = 0;
// var optAnswer = 0;
$(window).load( function()  {
	$.ajax({
		type: 'GET',
		url: '/quiz/obtener/'+$('.mainContainer').attr('data-id'),
		dataType: 'html',
		success: function (data) {
			quizArray = JSON.parse(data);
			initGame();
		}
	});
});
function initGame(){
	createQuiz();
	iniTemporizador();
	initDrags();
}
function createQuiz(){
	console.log( quizArray);
	$('.textCount').text('1/'+quizArray.quizz.cuestionario.length+'');
	progress = 100/quizArray.quizz.cuestionario.length;
	$('.containerBars .containerBar .dataBar .elementBar.titleQuiz').html(quizArray.quizz.nombreQuizz);
	$('.containerBars .containerBar .dataBar .elementBar.nomenclatureQuiz').html(quizArray.materia.tipo.toUpperCase());
	for(var i = 0; i < quizArray.quizz.cuestionario.length; i++){
		console.log( quizArray.quizz.cuestionario[i].tipo );
		switch(quizArray.quizz.cuestionario[i].tipo){
			// QUIZ ABIERTA
			case 'tipoT': 
				quizPreguntaImagen(i+1);
				break;
			// QUIZ RELACIONAL
			case 'tipoR':
				quizRelacional(i+1);
				break;
			// QUIZ PREGUNTA IMAGEN
			case 'tipoIT':
				quizPreguntaImagen(i+1);
				break;
			// QUIZ MATEMATICAS
			case 'tipoM':
				quizMatematicas(i+1);
				break;
			case 'tipoOM':
				quizOpcionMultiple(i+1);
				break;
			case 'tipoAP':
				quizArrastrarPalabra(i+1);
				break;
			case 'tipoSP':
				quizSeleccion(i+1);
				break;
		}
	}
    pantallaFinal( $('.quiz').length+1 );
}
function pantallaFinal(numQuiz){
	contentQuiz = '';
    console.log($(".mainContainer").attr("data-alumno"));
	contentQuiz = 	'<div class="quiz calificacion" data-type="" data-quiz="'+numQuiz+'">'+
						'<div class="contenedorCalificacion">'+
							'<div class="tituloCalificacion">¡Has finalizado!</div>'+
							'<div class="contenedorTiempo">'+
								'<div class="contenedorIcono">'+
									'<div class="iconoTiempo"></div>'+
								'</div>'+
								'<div class="tiempoCalificacion"></div>'+
							'</div>'+
							'<div class="mensajeCalificacion">Hasta aquí termina el Quizz,<br>envia tus respuestas<br><span>¡SUERTE!</span></div>'+
							'<div class="contenedorBoton">';
                            if($(".mainContainer").attr("data-alumno") != 0) 
                                contentQuiz += '<div class="btn botonCalificiacion">Enviar respuestas</div>';
                            else{
                                contentQuiz += '<div class="btn botonModificar">Modificar</div>';
                                contentQuiz += '<div class="btn botonAprobar">Aprobar</div>';
                            }
    contentQuiz +=			'</div>'+
						'</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
}
function quizOpcionMultiple(numQuiz){
	contentQuiz = '';
	if(quizArray.quizz.cuestionario[numQuiz-1].pregunta.length < 4){
		contentQuiz = 	'<div class="quiz trueFalse '+(!quizArray.quizz.cuestionario[numQuiz-1].img ? 'notImagen': '')+'" data-type="tipoOM" data-quiz="'+numQuiz+'">'+
							'<div class="containerExercise">'+
								'<div class="containerAnswer false">'+
									'<div class="botonAnswer">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[2]+'</div>'+
								'</div>'+
								'<div class="containerQuestion">'+
									'<div class="containerImg">'+
										'<div class="imgQuestion">'+
											'<img src="'+quizArray.quizz.cuestionario[numQuiz-1].imgRoute+'">'+
										'</div>'+
									'</div>'+
									'<div class="question">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
								'</div>'+
								'<div class="containerAnswer true">'+
									'<div class="botonAnswer">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[1]+'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
		$('.containerQuiz').append(contentQuiz);
		$('.quiz[data-quiz="'+numQuiz+'"]').find('.botonAnswer').on(clickHandler, function(){
			seleccionrOption( $(this),'opcionMultiple' );
		});
	}
	else{
		contentQuiz += 	'<div class="quiz opcionMultiple '+(!quizArray.quizz.cuestionario[numQuiz-1].img ? 'notImagen': '')+'" data-type="tipoOM" data-quiz="'+numQuiz+'">'+
							'<div class="containerExercise">'+
								'<div class="containerQuestion">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
								'<div class="containerElements">'+
									'<div class="containerImg">'+
										'<div class="imagen">'+
											'<img src="'+quizArray.quizz.cuestionario[numQuiz-1].imgRoute+'">'+
										'</div>'+
									'</div>'+
									'<div class="containerAnswers">';
										for(var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].pregunta.length -1; i++){
											contentQuiz += 	'<div class="containerAnswer">'+
																'<div class="containerRadius">'+
																	'<div class="radius"></div>'+
																'</div>'+
																'<div class="containerText">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[i+1]+'</div>'+
															'</div>';
										}
					contentQuiz += '</div>'+
								'</div>'+
							'</div>'+
						'</div>';
		$('.containerQuiz').append(contentQuiz);
		$('.quiz[data-quiz="'+numQuiz+'"]').find('.containerRadius').on(clickHandler, function(){
			seleccionrOption( $(this),'opcionMultiple' );
		});
	}
}
function quizMatematicas(numQuiz){
	contentQuiz = '';
	contentQuiz = 	'<div class="quiz matematicas" data-type="tipoM" data-quiz="'+numQuiz+'">'+
						'<div class="containerInstruction">'+(typeof(quizArray.quizz.cuestionario[numQuiz-1].instrucciones) !== "undefined" && quizArray.quizz.cuestionario[numQuiz-1].instrucciones !== null ? quizArray.quizz.cuestionario[numQuiz-1].instrucciones : '')+'</div>'+
						'<div class="containerQuestion">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
						'<div class="containerAnswer">'+
                            '<input class="inputAnswer" type="number" placeholder="Respuesta" >'+
                        '</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
}
function quizPreguntaAbierta(numQuiz){
	contentQuiz = '';
	contentQuiz = 	'<div class="quiz preguntaAbierta" data-type="tipoT" data-quiz="'+numQuiz+'">'+
						'<div class="containerInstruction">'+(typeof(quizArray.quizz.cuestionario[numQuiz-1].instrucciones) !== "undefined" && quizArray.quizz.cuestionario[numQuiz-1].instrucciones !== null ? quizArray.quizz.cuestionario[numQuiz-1].instrucciones : '')+'</div>'+
						'<div class="containerQuestion">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
						'<div class="containerAnswer">'+
							'<input class="inputAnswer" type="text" placeholder="Respuesta" >'+
						'</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
}
function quizPreguntaImagen(numQuiz){
	contentQuiz = '';
    if(quizArray.quizz.cuestionario[numQuiz-1].img){
        contentQuiz = 	'<div class="quiz preguntaImagen" data-type="tipoIT" data-quiz="'+numQuiz+'">'+
                            '<div class="containerImg">'+
                                '<img src="'+quizArray.quizz.cuestionario[numQuiz-1].imgRoute+'">'+
                            '</div>'+
                            '<div class="containerQuestion">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
                            '<div class="containerAnswer">'+
                                '<input class="inputAnswer" type="text" placeholder="Respuesta" >'+
                            '</div>'+
                        '</div>';
    }
    else{
        contentQuiz +=	'<div class="quiz preguntaImagen" data-type="tipoIT" data-quiz="'+numQuiz+'">'+
								'<div class="containerExercise">'+
									'<div class="containerQuestion">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[0]+'</div>'+
									'<div class="containerAnswer">'+
										'<input class="inputAnswer" type="text" placeholder="Responder" >'+
									'</div>'+
								'</div>'+
                            '</div>'+
                        '</div>';
    }
	$('.containerQuiz').append(contentQuiz);
}
function quizRelacional(numQuiz){
	contentQuiz = '';
	contentQuiz = 	'<div class="quiz relacional" data-type="tipoR" data-quiz="'+numQuiz+'">'+
						'<div class="containerExercise">'+crearOption(numQuiz)+'</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
	$('.cotainerOptions .cotainerOption').on(clickHandler, function(){
		seleccionrOption( $(this),'relacional' );
	});
}
function quizArrastrarPalabra(numQuiz){
	contentQuiz = '';
	contentQuiz = 	'<div class="quiz ArrastrarPalabra '+(!quizArray.quizz.cuestionario[numQuiz-1].img ? 'notImagen': '')+'" data-type="tipoAP" data-quiz="'+numQuiz+'">'+
						'<div class="elementsExercise">'+
							'<div class="containerElements">'+
								'<div class="containerImg">'+
									'<img src="'+quizArray.quizz.cuestionario[numQuiz-1].imgRoute+'">'+
								'</div>'+
								'<div class="containerDrags">'+crearDrags(numQuiz)+'</div>'+
							'</div>'+
						'</div>'+
						'<div class="containerExercise">'+crearExercise(numQuiz,'ArrastrarPalabra')+'</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
}
function quizSeleccion(numQuiz){
	contentQuiz = '';
	contentQuiz = 	'<div class="quiz SeleccionPalabra '+(!quizArray.quizz.cuestionario[numQuiz-1].img ? 'notImagen': '')+'" data-type="tipoS" data-quiz="'+numQuiz+'">'+
						'<div class="elementsExercise">'+
							'<div class="containerElements">'+
								'<div class="containerImg"></div>'+
								'<div class="containerText">'+
									'<p>'+quizArray.quizz.cuestionario[numQuiz-1].instrucciones+'</p>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="containerExercise">'+crearExercise(numQuiz,'SeleccionPalabra')+'</div>'+
					'</div>';
	$('.containerQuiz').append(contentQuiz);
	crearSelect(numQuiz);
	if( $('.quiz[data-quiz="'+numQuiz+'"] .containerText p').text().length <= 0 ){
		$('.quiz[data-quiz="'+numQuiz+'"]').addClass('notInstruction');
	}
}
function crearDrags(numQuiz){
	let contentDrag = '';
	for( var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].respuesta.length; i++ ){
		for( var a = 0; a < quizArray.quizz.cuestionario[numQuiz-1].respuesta[i].correctas.length; a++ ){
			contentDrag +=	'<div class="containerDrag">'+
								'<div class="drag">'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[i].correctas[a][0].resp+'</div>'+
							'</div>';
		}
		if( quizArray.quizz.cuestionario[numQuiz-1].respuesta[i].incorrectas ){
			for( var a = 0; a < quizArray.quizz.cuestionario[numQuiz-1].respuesta[i].incorrectas.length; a++ ){
				contentDrag +=	'<div class="containerDrag">'+
									'<div class="drag">'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[i].incorrectas[a]+'</div>'+
								'</div>';
			}
		}
	}
	return contentDrag;
}
function crearExercise(numQuiz,objQuiz){
	let contentExcercise = '';
	if(objQuiz == 'ArrastrarPalabra'){
			let contentTarget = '<div class="containerTarget"><div class="drop-target"></div></div>';
			for(var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].pregunta.length;i++ ){
				contentExcercise += '<div class="exercise" index="'+(i+1)+'">'+
										'<div class="containerNumber">'+
											'<div class="number">'+(i+1)+'</div>'+
										'</div>'+
										'<div class="textExercise">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[i].replace(/_/g,contentTarget)+'</div>'+
									'</div>'; 
			}
	}
	else{
		let contentSelect = '<div class="containerSelect"><select></select></div>';
		for( var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].pregunta.length;i++ ){
			contentExcercise += '<div class="exercise">'+
									'<div class="containerNumber">'+
										'<div class="number">'+(i+1)+'</div>'+
									'</div>'+
									'<div class="textExercise">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[i].replace(/_/g,contentSelect)+'</div>'+
								'</div>';
		}
	}
	return contentExcercise;
}
function crearSelect(numQuiz){
	console.log('entro');
	$('.quiz[data-quiz="'+numQuiz+'"] .exercise').each(function(indexEcercise){
		$(this).find('.containerSelect').each(function(indexSelect) {
			$(this).children('select').append('<option vlue="'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[indexEcercise].correctas[indexSelect][0].resp+'">'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[indexEcercise].correctas[indexSelect][0].resp+'</option>');
			for( var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].respuesta[indexEcercise].incorrectas[indexSelect].length; i++ ){
				$(this).children('select').append('<option value="'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[indexEcercise].incorrectas[indexSelect][i].resp+'">'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[indexEcercise].incorrectas[indexSelect][i].resp+'</option>');
			}
			randomeSelect( $(this).children('select') );
		});
    });
}
function randomeSelect( objSelect ){
	randome = objSelect.children('option');
	randomeArray(randome);
	objSelect.children('option').remove();
	objSelect.append(randome);
}
function crearOption(numQuiz){
	let arrQuestion = [];
	let arrAnswer = [];
	let questionAleatorio;
	let answerAleatorio;
	let totalAleatorio = quizArray.quizz.cuestionario[numQuiz-1].pregunta.length;
	let contentOption = '';
	for(var i = 0; i < totalAleatorio;){
		questionAleatorio = Math.floor((Math.random() * totalAleatorio) + 1);
		if(arrQuestion.indexOf(questionAleatorio) == -1 ){
			arrQuestion.push(questionAleatorio);
			i++;
		}
	}
	for(var i = 0; i < totalAleatorio;){
		answerAleatorio = Math.floor((Math.random() * totalAleatorio) + 1);
		if(arrAnswer.indexOf(answerAleatorio) == -1 ){
			arrAnswer.push(answerAleatorio);
			i++;
		}
	}
	for(var i = 0; i < quizArray.quizz.cuestionario[numQuiz-1].pregunta.length; i++){
		var indexQuestion = quizArray.quizz.cuestionario[numQuiz-1].pregunta.indexOf( quizArray.quizz.cuestionario[numQuiz-1].pregunta[ arrQuestion[i]-1 ] );
		var indexAnswer = quizArray.quizz.cuestionario[numQuiz-1].respuesta.indexOf( quizArray.quizz.cuestionario[numQuiz-1].respuesta[ arrAnswer[i]-1 ] );
		contentOption += 	'<div class="cotainerOptions">'+
								'<div class="cotainerOption question" data-opcion="'+(indexQuestion+1)+'">'+
									'<div class="containerNumber">'+
										'<div class="number">'+(i+1)+'</div>'+
									'</div>'+
									'<div class="containerText">'+quizArray.quizz.cuestionario[numQuiz-1].pregunta[ arrQuestion[i]-1 ]+'</div>'+
								'</div>'+
								'<div class="cotainerOption answer" data-opcion="'+(indexAnswer+1)+'">'+
									'<div class="containerNumber">'+
										'<div class="number"></div>'+
									'</div>'+
									'<div class="containerText">'+quizArray.quizz.cuestionario[numQuiz-1].respuesta[ arrAnswer[i]-1 ]+'</div>'+
								'</div>'+
							'</div>';
	}
	return contentOption;
}
function seleccionrOption(objOption,tipoQuiz){
	if( tipoQuiz == 'relacional' ){
		if( objOption.hasClass('validado') ){
			$('.number:contains("'+objOption.find('.number').text()+'")').parents('.cotainerOption').removeClass('validado');
			$('.number:contains("'+objOption.find('.number').text()+'")').parents('.cotainerOption').removeAttr('data-respuesta');
			// $('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.select').attr('data-respuesta',$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.question.select .number').text() );
		}
		else{
			if( objOption.hasClass('select') ){
				objOption.removeClass('select');
				contadorSeleccion--;
			}
			else{
				if( objOption.hasClass('question') ){
					if( $('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.question.select').length == 1 ){
						contadorSeleccion--;
					}
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.question').removeClass('select');
				}
				else{
					if( $('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.answer.select').length == 1 ){
						contadorSeleccion--;
					}
					objOption.find('.number').text('');
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.answer').removeClass('select');
				}
				objOption.addClass('select');
				contadorSeleccion++;
				if( contadorSeleccion == 2 ){
					contadorSeleccion = 0;
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.select').addClass('validado');
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.select').attr('data-respuesta',$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.question.select .number').text() );
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.answer.select .number').text( $('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.question.select .number').text() );
					$('.quiz[data-quiz="'+contadorQuiz+'"] .cotainerOption.validado').removeClass('select');
				}
			}

		}
	}
	else if( tipoQuiz == 'opcionMultiple' ){
		if( !objOption.hasClass('select') ){
			console.log('entra');
			$('.quiz[data-quiz="'+contadorQuiz+'"] .botonAnswer').removeClass('select');
			$('.quiz[data-quiz="'+contadorQuiz+'"] .containerRadius').removeClass('select');
			objOption.addClass('select');
		}
	}
}
function initDrags(){
	randomeArrastrable();
	$('.drag').pep({
		droppable: '.drop-target',
		allowDragEventPropagation: false,
		overlapFunction: false,
		useCSSTranslation: false,
		revertAfter: true,
		overlapFunction: false,
		startPos: { left: 0, top: 0 },
		cssEaseDuration: 0,
		initiate: function(ev, obj){},
		// start: function(ev, obj){ getAudio(obj.$el);obj.noCenter = false},
		drag: function(ev, obj){onTouchDrag(obj.$el);},
		rest: function(ev, obj){handleOnDrop(ev, obj, obj.$el)}
	});
}
function randomeArrastrable(){
	randome = $('.containerDrags .containerDrag');
	randomeArray(randome);
	$('.containerDrags .containerDrag').remove();
	$('.containerDrags').append(randome);
}
randomeArray = function(o) {
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}
function onTouchDrag(){
	$('.containerTarget').addClass('active');
}
function handleOnDrop(ev, obj, $obj){
	if ( (obj.activeDropRegions.length == 1 ) && $('.containerTarget').hasClass('active')) {
		$obj.addClass('validado');
		returnDrag($obj);
		console.log( $obj.parent('.containerDrag').index() );
		$('.pep-dpa').parent('.containerTarget').attr('data-drag',$obj.parent('.containerDrag').index());
		$('.pep-dpa').parent('.containerTarget').addClass('validado');
		$('.pep-dpa').parent('.containerTarget').html( $obj.html() );
		$('.containerTarget.validado').on(clickHandler, function(){
			removeDrag( $(this) );
		});
	}
	else{
		returnDrag($obj);
	}
	$('.containerTarget').removeClass('active');
}
function returnDrag(objDrag){
	objDrag.animate({
		left: 0, 
		top: 0
	},500);
}
function removeDrag( objTarget ){
	objTarget.removeClass('validado');
	objTarget.html('');
	objTarget.append('<div class="drop-target"></div>');
	$('.quiz[data-quiz="'+objTarget.parents('.quiz').attr('data-quiz')+'"] .containerDrag').eq( objTarget.attr('data-drag') ).children().removeClass('validado')
	objTarget.removeAttr('data-drag');
}