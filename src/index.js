
//--- Motor Universal del Proyecto----//
var http=require('http');
const express= require('express');
const app= express();
const path= require('path');
const mongoose= require('mongoose');
const passport= require('passport');
const session=require('express-session');
require('dotenv').config();
const fileUpload = require('express-fileupload');

const server=http.createServer(app);

const methodOverride=require('method-override');


require('./config/passport');
//Settings
//app.set('port',3000);
app.set('port',process.env.PORT||8443);

app.set('views',path.join(__dirname,'views'));

//Motor de Plantillas
app.set('view engine','ejs');


//Middleware
app.use(methodOverride('_method'));
app.use(session({
   secret: 'mysecretapp',
   resave:true,
   saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(fileUpload());

//Routes
app.use(require('./routes/index'));
app.use('/quiz', require('./routes/quiz'));
app.use('/alumnos', require('./routes/alumnos'));

app.use(express.static(path.join(__dirname, 'public')));


//Variables Globales
app.use((req,res,next)=>{
   
   res.locals.user=req.user || null;
   //console.log(res.locals.user);
   next();
});

server.listen(app.get('port'),()=>{
   console.log("Servidor funcionando en puerto:"+ app.get('port'));
});
//Conexión a la BD
const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_CLUSTER}/${process.env.DB_DBNAME}?retryWrites=true&w=majority`;
mongoose.connect(uri,{useNewUrlParser: true, useUnifiedTopology: true } ,(err, res) =>{
   if (err) throw err
  console.log("Conexion con Mongo establecida");
})

