const express = require('express');
const router = express.Router();


const API=require("../funciones/api.js");
/* MODEL */
const Quizz = require('../modelos/quizz.js');
const Materia = require('../modelos/materia.js');

// router.get('/visualizar/:idQuizz', async (req, res) => {
//     var quizz = await Quizz.findById(req.params.idQuizz).lean();
//     var materia= await API.findByID("materias",quizz.materia);
//     materia=materia.nombre;
//     res.render('quiz/quiz',{materia,quizz});
// });
router.get("/obtener/:idQuizz", async(req, res) => {
    try {
        var quizz= await Quizz.findById(req.params.idQuizz).lean();
        var materia= await API.findByID("materias",quizz.materia);
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify({materia,quizz}));
        res.end();
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.get('/newQuizz', async (req, res) => {
    res.render('quiz/new_quiz.ejs',{"quiz":{tipo:"new"}} );
});
router.get('/editQuizz/:idQuizz', async (req, res) => {
    var quizz = await Quizz.findById(req.params.idQuizz).lean();
    // console.log(quizz);
    res.render('quiz/new_quiz.ejs',{ "quiz": {tipo:"edit", idQuizz:req.params.idQuizz } });
});
router.post('/saveNewQuizz', async (req, res) => {
    // console.log("data: ",req.body.quiz);
    var nuevoQuizz= await Quizz.create(
        req.body.quiz
    );   
    var link="/visualizar/"+nuevoQuizz.id;
    res.writeHead(200, {"Content-Type": "application/json"});
    res.write(JSON.stringify({'status': 'ok','redirect':link}));
    res.end();
    // res.redirect(link);
});
router.post('/saveEditQuizz/:idQuiz', async (req, res) => {
    // console.log("data: ",req.body.quiz);
    const idQuizz = req.params.idQuiz;
    var editQuizz = Quizz.findByIdAndUpdate(idQuizz, req.body.quiz, (error, user) => {
        console.log("Error")
        console.log(error, idQuizz);
    });  
    var link="/visualizar/"+idQuizz;
    res.writeHead(200, {"Content-Type": "application/json"});
    res.write(JSON.stringify({'status': 'ok','redirect':link}));
    res.end();
    // res.redirect(link);
});

module.exports = router;