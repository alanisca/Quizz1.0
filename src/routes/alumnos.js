const express = require('express');
const router = express.Router();

const Funciones=require("../funciones/funciones.js");
/* MODEL */
const Quizz = require('../modelos/quizz.js');

router.get('/examen/:id/alumno/:idAlumno/materia/:materia', async (req, res) => {
    const idAlumno = req.params.idAlumno;
    const quizz = await Quizz.findById(req.params.id);
    //Optimización de Cuestionarios//
    /*var quizPrueba=await Quizz.findById(req.params.id).lean();
    console.log(quizPrueba.nombreQuizz);
    */
    var contadorCuestionarios=quizz.cuestionario.length;

    var clavesCuestionarios=[];
    for(var i=0;i<contadorCuestionarios;i++){
        clavesCuestionarios.push(i);
    }

    //Variable para mostrar los cuestionarios en un orden diferente a los alumnos
    clavesCuestionarios=Funciones.shuffle(clavesCuestionarios);

    var materia=req.params.materia;
    // res.render('alumnos/Quizz', { materia,quizz,clavesCuestionarios, idAlumno });
    res.render('quiz/quiz',{materia,quizz,idAlumno});
});
module.exports = router;