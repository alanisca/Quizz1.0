const express = require('express');
const router = express.Router();
const axios = require('axios');

const nuevoArchivo = async () => {
    try{
        const respuesta = await axios.get("https://api.jenios.mx/jenios/v1.0.0/proyectos/bucket",{
            headers: {'Authorization': 'Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng'},
        });
        // const respuesta = await axios.get("https://reqres.in/api/users?page=2");
        return respuesta.data;
    }catch(err){
        return err;
    }
} 
router.post("/getNewFileRoute", async (req, res) => {
    const data_file = await nuevoArchivo()
    console.log("data_file_url",data_file.url);
    console.log("ARCHIVO: ",req.files);
    let sampleFile;
    let uploadPath;

    if (!req.files.archivo || !req.files || Object.keys(req.files).length === 0) {
        res.status(400).send({'msg':'Es necesario enviar un archivo'});
        return;
    }
    const { archivo } = req.files;
    uploadPath = data_file.url+"/"+archivo.name;
    try{
        const respuesta = await axios.get(data_file.url,{
            headers: {'Authorization': 'Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng'},
        });
        // const respuesta = await axios.get("https://reqres.in/api/users?page=2");
        return respuesta.data;
    }catch(err){
        console.log({"Error":err});
        return res.status(500).send(err);
    }
    archivo.mv(uploadPath, (err) => {
        if (err) {
        }
        else{
            console.log({msg:'Archivo cargado en' + uploadPath, URL: uploadPath});
            res.send({msg:'Archivo cargado en' + uploadPath, URL: uploadPath});
        }
    });
});
/** Rutas para subír imágenes **/
router.post("/upload/");
module.exports = router;

/*
{
    "type": "success",
    "status": 201,
    "message": "La petición se ha realizado correctamente",
    "images": [
        {
            "id": "Vc5sds0WX",
            "src": "https://f1.s2.jenios.mx/uploads/Vc5sds0WX.png",
            "en_uso": 0,
            "descripcion": "",
            "farm": "1",
            "server": "2"
        }
    ]
}
{
    "type": "success",
    "status": 201,
    "message": "La petición se ha realizado correctamente",
    "images": [
        {
            "id": "p3DOC6OtU",
            "src": "https://f1.s2.jenios.mx/uploads/p3DOC6OtU.png",
            "en_uso": 0,
            "descripcion": "",
            "farm": "1",
            "server": "2"
        }
    ]
}*/