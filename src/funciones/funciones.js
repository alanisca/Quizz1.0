'use strict'
const Imagenes = require('../modelos/imagenes.js');
const Chunks = require('../modelos/chunks.js');
const { ObjectId } = require('mongodb');
const axios = require('axios');
const FormData = require('form-data');
//Función para revolver los elmentos de un array.
exports.shuffle= function(array) {
    var currentIndex = array.length
            , temporaryValue
            , randomIndex
            ;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
   
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

//----------------FUNCIONES PARA IMAGENES---------------//
//Función para buscar imagenes en base a su nombre original en el array de imagenes.
//Se aplica para la creación de examenes
exports.buscarImagen=function(imagenBuscada,arrayImagenes){
    var i;
    console.log("buscar Imagen");
    console.log(imagenBuscada,arrayImagenes);
    for( i in arrayImagenes){
        if(imagenBuscada==arrayImagenes[i].originalname){
            return arrayImagenes[i];
        }
    }   
}

exports.eliminarImagen = async function(nombreImagen){
    console.log(nombreImagen)
    var  imagen= await Imagenes.find({filename: nombreImagen}).exec();
    
    
    if(!imagen[0]){
        console.log("Error no hay ID de Imagen definida");
    }
    else{
        var imagenID=imagen[0]._id;
        //Borrado de los Chunks que pertenecen a una imagen
        Chunks.deleteMany({files_id: {$eq: ObjectId(imagenID)}}, function(err, result) {
            if (err) {
              console.log(err);
            } else {
              console.log("Imagen Borrada con éxito");
            }
          });
    }
    
    //Borrado de la imagen
    await Imagenes.deleteOne({filename: nombreImagen});
}
var getImageUrl = async () => {
    try{
        const respuesta = await axios.get("https://api.jenios.mx/jenios/v1.0.0/proyectos/bucket",{
            headers: {'Authorization': 'Jenios eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGF2ZV9wdWJsaWNhIjoiRkNrVTFvVXhBd0gyQ0NXOUszR2ZMWWdFQlZYSjBnYTMiLCJpYXQiOjE2MDY1ODIxNjIsImF1ZCI6Imh0dHBzOi8vZWJlLmplbmlvcy5teCJ9.FdzBf6ZGyH6ViwFTXpP2hzHzawhPmyGtRjTWmpuG_Ng'},
        });
        return respuesta.data;
    }catch(err){
        console.log("Error al solicitar url: "+err);
        return err;
    }
}
exports.newImage = async (req) => {
    const data_file = await getImageUrl();
    const uploadPath = data_file.url;
    var bodyFormData = new FormData();
    bodyFormData.append('file', req.data,{
        filename: req.name,
        contentType: req.mimetype,
        knownLength: req.size
    });
    const formHeaders = bodyFormData.getHeaders();
    return new Promise((resolve, reject) => {
        axios({
            method: "post",
            url: uploadPath+"/api/v1.0.0/ebe/img/quiz",
            data: bodyFormData,
            headers: { 
                ...formHeaders,
                "Authorization": process.env.header_Authorization
            },
        }).then(function (response) {
            //handle success
            console.log("CORREEEECTO: ");
            console.log(response.data);
            resolve(response.data);
        }).catch(function (err) {
            //handle error
            console.log("Error al guardar: "+err);
            resolve(err);
        });
    });
}
exports.deleteImage = async (id_image) => {
    const data_file = await getImageUrl();
    const uploadPath = data_file.url;
    return new Promise((resolve, reject) => {
        axios({
            method: "post",
            url: uploadPath+"/api/v1.0.0/ebe/img/delete/quiz",
            data: {
                "photo_id": id_image,
                "public_key": process.env.public_key
            },
            headers: {
                "Authorization": process.env.header_Authorization
            },
        }).then(function (response) {
            console.log("CORREEEECTO: ");
            console.log(response.data);
            resolve(response.data);
        }).catch(function (err) {
            console.log("Error al guardar: "+err);
            resolve(err);
        });
    });
}
/*

*/